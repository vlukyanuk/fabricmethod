package com.skillsup.fabricMethod.factory;

import com.skillsup.fabricMethod.buttons.Button;
import com.skillsup.fabricMethod.buttons.WindowsButton;

public class WindowDialog extends Dialog {
    public Button createButton() {
        return new WindowsButton();
    }
}
