package com.skillsup.fabricMethod.factory;

import com.skillsup.fabricMethod.buttons.Button;
import com.skillsup.fabricMethod.buttons.HtmlButton;

public class HtmlDialog extends Dialog {

    public Button createButton() {
        return new HtmlButton();
    }
}
