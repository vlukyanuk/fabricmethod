package com.skillsup.fabricMethod.factory;

import com.skillsup.fabricMethod.buttons.Button;

public abstract class Dialog {
    public void renderWindow() {
        Button button = createButton();
        button.render();
    }

    public abstract Button createButton();
}
