package com.skillsup.fabricMethod.buttons;

public interface Button {
    void render();
    void onClick();
}
